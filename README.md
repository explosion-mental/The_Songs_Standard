The Songs Standard
==================

**NOTE:** this is unstable and is subject to change. Wait for version 1.0

About
-----
This is a simple and obvious standard that programs *should* conform to in order
to coexist and reuse the same data.

The objective of this standard is to have a nice structured way in which
applications can fetch metadata in a local manner, avoiding doing the same job
twice, enforcing compatibility between programs.

By having a great structure we can avoid embedded data in the audio file itself,
thus saving storage and keeping only the relevant metadata. Contrary to embeding
data, complementary files would require a separate directory, if more than one
file are required in that category.

Below are the current valid categories:

Lyrics
------
These files should use ~.txt~ file extension.

Images
------
These are files are images.

Covers
------
Covers are usually on a *per album* basis, but it is possible to assign

Artist
------
To identify an artist it is up to the embedded metadata of the audio file to
*display the appropriate name with proper symbols in the proper order*

More?
-----
Adding more categories to this can be done in a backwards compatible way, so it
can be done as extensions if the format can be read as a **plain text file**
like the [lyrics](#Lyrics).



